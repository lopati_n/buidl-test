var fs = require('fs')
var createHTML = require('create-html')

var html = createHTML({
    head: process.env.CI_COMMIT_SHA,
    body: '<p>' + new Date() + '</p>'
})

fs.writeFile('dist/ver.html', html, function (err) {
  if (err) console.log(err)
})

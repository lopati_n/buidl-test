FROM node:10 as build

WORKDIR /usr/src/app

ARG CI_COMMIT_SHA
ENV CI_COMMIT_SHA $CI_COMMIT_SHA

COPY package.json /usr/src/app/
RUN npm install

COPY . /usr/src/app

RUN npm run build

CMD [ "npm", "start" ]
